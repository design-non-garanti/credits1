floodEnds={
	'default':[
	function(scale,options){
		if(scale.floods[options.id])return false
			return true
	},
	function(scale,options){
		if(!options.lifeTime)return false
			return true
	},
	function(scale,options){
		for(var s in options.scope)if(scale.datas[options.scope[s]])return false
			return true
	}],
}

floodFills={
	'default':{
		lifeTime:Infinity,
		dir:[[-1,0,0],[1,0,0],[0,-1,0],[0,1,0]],
		floodEnd:floodEnds['default'],
		speed:50,
		scope:[],
		bool:true,
		parsed:true,
	}
}

animations={
	'jump':function(mobile,dir){
		var position = mobile.position;
		mobile.position.floodFill({data:'jump',speed:17,lifeTime:mobile.jump,dir:[dir]});
		setTimeout(function(){
			position.floodFill({data:'jump',bool:false,speed:17,lifeTime:mobile.jump,dir:[dir]});
		},100);
	},
	'light':function(mobile){
		var position = mobile.position;
		mobile.position.floodFill({data:'night',bool:false,speed:1,lifeTime:16});
		light(mobile,position);
	}
}

function light(mobile,position){
	if(mobile.position.position-position.position<8 && mobile.position.position-position.position>-8
		&& mobile.position.context[1].position-position.context[1].position<8 && mobile.position.context[1].position-position.context[1].position>-8){
		setTimeout(function(){
			light(mobile,position);
		},100);
	}else{
		position.floodFill({data:'night',scope:['player'],bool:true,speed:1,lifeTime:16});
	}
}

keys={
	'default':{
		'1':function(jump){return[-jump,jump,0]},
		'2':function(jump){return[0,jump,0]},
		'3':function(jump){return[jump,jump,0]},
		'4':function(jump){return[-jump,0,0]},
		/*'5':function(jump){return[0,0,0]},*/
		'6':function(jump){return[jump,0,0]},
		'7':function(jump){return[-jump,-jump,0]},
		'8':function(jump){return[0,-jump,0]},
		'9':function(jump){return[jump,-jump,0]},
		'z':function(jump){return[0,-jump,0]},
		'q':function(jump){return[-jump,0,0]},
		's':function(jump){return[0,jump,0]},
		'd':function(jump){return[jump,0,0]},
		'ArrowUp':function(jump){return[0,-jump,0]},
		'ArrowLeft':function(jump){return[-jump,0,0]},
		'ArrowDown':function(jump){return[0,jump,0]},
		'ArrowRight':function(jump){return[jump,0,0]},
	}
}

setups={
	'default':function(){
		matrix = new Scale({dom:'main',class:'matrix'});
		matrix.buildIn(1,{class:'layer',dom:'table'});
		for(var la in matrix.layer)matrix.layer[la].buildIn(24,{class:'line',dom:'tr'});
		for(var la in matrix.layer)for(var l in matrix.layer[la].line)matrix.layer[la].line[l].buildIn(48,{class:'type',dom:'td'})
		for(var la in matrix.layer){
			for(var l in matrix.layer[la].line){
				for(var t in matrix.layer[la].line[l].type){
					matrix.layer[la].line[l].type[t].dom.textContent=' ';
					matrix.layer[la].line[l].type[t].fill({data:'night'});
				}
			}
		}
		return matrix
	}
}

function floodEnding(scale,options,floodEnd,i){
	if(!i)i=0;
	if(i>floodEnd.length-1)return true
	if(!floodEnd[i](scale,options))return false;
	i++;
	return floodEnding(scale,options,floodEnd,i);
}

function parseFloodFill(options,setting){
	if(!setting)setting=floodFills['default'];
	fill=define({},options,setting);
	return fill;
}