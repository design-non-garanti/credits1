Mobile = function(options){
	mobile=this;
	define(this,options,{
		class:'mobile',
		jump:1,
		moveFlood:{scope:['wall']},
		moveKeys:keys['default'],
	});
	onKeyDownEvents.push(function(e){
		mobile.events(e);
	})
	this.move('6');
	this.move('4');
}

Mobile.prototype.events = function(e) {
	if(this.moveKeys[e.key])this.move(e.key);
}

Mobile.prototype.move = function(key){
	animations['light'](this,this.moveKeys[key](1));
	dir=this.moveKeys[key](this.jump);
	this.moveFlood.data=this.class;
	this.moveFlood.bool=true;
	this.moveFlood.id=false;
	relay = this.position.findRelay(dir);

	if(relay && relay.fill(this.moveFlood)){
		animations['jump'](this,this.moveKeys[key](1));
		this.moveFlood.bool=false;
		this.moveFlood.id=false;
		this.position.fill(this.moveFlood);
		this.position=relay;
	}

}