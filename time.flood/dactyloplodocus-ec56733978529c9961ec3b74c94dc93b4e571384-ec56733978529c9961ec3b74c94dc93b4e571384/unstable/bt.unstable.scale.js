Scale = function(options){
	define(this,options,{
		datas:{},
		context:[],
		floods:{}
	});
	this.context=[this].concat(this.context);
	this.datas[this.class]=true;
	if(this.dom)this.createDOM();
}

Scale.prototype.buildIn = function(howmany,what){
	if(!this[what.class])this[what.class]=[];
	what.context=this.context;
	what.position=what.context[0][what.class].length;
	this[what.class].push(new Scale(what));
	howmany--;
	if(!howmany)return true;
	return this.buildIn(howmany,what);
}

Scale.prototype.fill = function(options){
	if(!options.parsed)options=parseFloodFill(options);
	if(!options.id)options.id=Math.random();
	if(!floodEnding(this,options,options.floodEnd))return false
	this.floods[options.id]=true;
	if(options.data)this.datas[options.data]=options.bool;
	if(this.dom)this.updateDOM();
	return true
}

Scale.prototype.floodFill = function(options){
	if(!options.parsed)options=parseFloodFill(options);
	if(!this.fill(options))return false
			options.lifeTime--;
		var relays=[];
	for(var d in options.dir)if(this.findRelay(options.dir[d]))relays.push(this.findRelay(options.dir[d]));
		setTimeout(function(){
			for(var r in relays)relays[r].floodFill(options);
		},options.speed);
	return true
}

Scale.prototype.findRelay = function(dir,dim,position){
	if(!dim)dim=0;
	if(dim>dir.length-1)return position
		var parent = this.context[this.context.length-dim-1][this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position];
	if(parent){
		if(!position){
			position=parent;
		}else{
			position=position[this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position];
		}
	}else{
		return false
	}
	dim++;
	return this.findRelay(dir,dim,position)
}