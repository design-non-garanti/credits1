var onKeyDownEvents=[];
var onKeyUpEvents=[];

document.onkeydown=function(e){
    for(var i in onKeyDownEvents){
        onKeyDownEvents[i](e);
    }
}

document.onkeyup=function(e){
    for(var i in onKeyUpEvents){
        onKeyUpEvents[i](e);
    }
}

function pick(from,to) {
    if(from.inventory.length>from.lock){
        data=from.inventory[from.inventory.length-1];
        if(to[data]){
            return false
        }
        from.inventory.splice(from.inventory.length-1, 1);
        return data
    }
    return false
}

var faces = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

function define(objet,params,defaults){
    if(!objet)objet={};
    if(!params)params={};
    if(!defaults)defaults={};
    for(var i in defaults){
        if(params[i]){
            objet[i]=params[i];
        }else{
            objet[i]=defaults[i];
        }
    }
    for(var i in params){
        objet[i]=params[i];
    }
    //console.log(objet);
    return objet
}

Keys = function(){}

Keys.prototype.trigger = function(key,down){
    this[key]=down;	
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function random(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min)
	}
}

Loop = function(context){
    this.context=context;
    this.stop=true;
    this.auto=true;
    this.key='F2';
    this.event="";
    this.delay=2000;
    this.play();
    var _loop =this;
    onkeydown_list.push(function(e){
        //console.log('manual_trigger',e);
        if(e.key==_loop.key && !_loop.auto){
            _loop.play();
        }
    })
    
}

Loop.prototype.break = function(){
    if(this.stop){
        this.stop=false;
        this.play();
    }else{
        this.stop=true;
    }
}

Loop.prototype.play = function(){
    if(this.stop)return
        if(typeof this.event == "string"){
            eval(this.event);
        }else{
            this.event();
        }
        var _loop=this;
        if(this.auto){
            setTimeout(function() {
                _loop.play();
            },this.delay);
        }
    }
