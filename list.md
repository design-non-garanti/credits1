# Design non-garanti

*design non-garanti*

- chercher à garantir
- présuppose d'un usage 
- heurte à l'individu / utilisateur
- absurde
- design non-garanti
- embrasse l'incertitude

*4 couples*

- théorie / pratique
- espace de travail / espace de monstration
- travail quotidien / projet de diplôme
- courte durée / longue durée

*monstration*

- objets utilisés
- site / serveur

## server

- mise en place d'un serveur
- à la fois un espace de travail et espace de montration (git, fichiers numériques, etc.)
- en ce sens s'oppose à la distinction entre et entraîne un flou entre ces deux espaces, concepts, idéaux, illusions
- rapport moins débordé, dépassé, une certaine proximité, rapport avec l'objet qui est de l'ordre de la possesion et de l'entretien
- une pratique alternative qui s'inscrit dans la décentralisation

* page d'accueil
* flyer 

## floodfill

- algorithme recursif de remplissage
- multi-curseurs, multi-grid, dimension temporelle, paramétrable

- typiquement concert évité comme moment de partage
- problème avec la représentation, l'exposition, le spectacle
- malgré le projet intéressant contexte qui n'est pas celui qu'on recherche
- live-coding projeté sur un crucifix accroché à de l'agglo pendant un concert
  d'orchestre symphonique dans une église devant des mamies et un maire de
  droite

* weaver.html
* multdim.html
* time.html
* video trames.flood.mp4

## dimensions

* étagères
* vomis
* words

## baptiste11ans

- workshop linda van deursen 
- une photo trouvé dans un livre / 1er resultat d'un moteur de recherche
- photographies perdue, cachée, oubliée / accès algorithmique contenu
- pencher sur la pertinence des contenu, qu'est ce qui la définie
- le web, ces acteurs, différentes méthodes de classification généralement basé sur le quantitatif et la proximité
- Google et son pageRanking, Youtube et ses suggestions
- parmis les grosses plateformes (réseaux sociaux (facebook, instagram, etc.), contenus (youtube, flickr))
- choisi Instagram car il traite principalement de l'image est dispose de compteurs (like, vues) 
- comme compteurs permette compter, il permette la quantification
- qu'en est il des images pas aimé, pas pertiente
- déplacement de la manière de montrer des images
- confusion entre texte et images
- commentaires et images

- 2 bots
- bot basé base de donnée de 100 000 utilisateurs que nous avons récupéré
- bot basé sur les liens entre utilisateurs (followers)

* bot
* active archive
* ascii

## terminator

- c'est un tueur de terme
- suite à des différentes lectures
- intérêt pour discours
- outil

* gooddesignisgood.html
* computer-technology.html


## recursive message system

- simple système de commentaire recursif
- library ajouter sur un
- dimension
- potentiel rédactionel / écriture
- détournement de l'usage des commentaires
- refaire des choses évidentes acquises mais en tirer autre chose

* 21e

## wayback machine screenshooter

- simple outil qui permet de prendre des screenshots des versions d'un site archivées sur archive.org
- permet d'avoir un aperçu global
- json en input permet de préciser des options

## fb-writer

- *nix architecture /dev/fb0 représentation de la matrice de pixels de l'écran
- inscrire directement du contenu dans ce fichier modifie directement l'affichage
- quelques calcul permette de mettre en place
- on retrouve un rapport assez direct à la manipulation des pixels de l'écran 

- empilement de couches

- pauvre console moche et pas graphique et mal aimé pas efficace
- refaire choses évidentes, points, lignes, etc.

## plenty of room

- version online utilisable (framapad, yourworldoftext, etc.)
- version téléchargeable, modifiable, etc.

* jacques bertin videos

## p2p-readwrite-web

- idée

## notes diverses

- inversion et déplacement défait
- défaire des discours, des acquis
- défaire des contrôles

- pas questions, pas de choix
- implications plus profondes que ce qu'il prétend être
- simplicité vers automatisation vers contrôle
- complexité et étrangeté
- dépassé par la simplicité 
- incertain


- simple et intelligent
- complexe et stupide


- faire des programmes
- faire des outils
- face à un paradoxe
- entre proposer quelque chose et imposer quelque chose
- impossibile de faire un outil qui conditionne pas un usages
- possibilité de faire un outil ouvert ou plutôt qui ouvre
- laisse émerger des usages et idées innatendus, non-prévues
- mediation et usages
- outils ne propose pas que des fonctionnalités il propose des idées, des
  idéaux, des manière de voir, de faire


- acquis
- oublie une situation diversifiable 

## (conclusion)

- hormis le projet dactyloplodocus durant les concerts romans / valence
- pas de présentation public, d'échanges, de commentaires, retours, etc. externes
- problème avec la représentation, l'exposition, le spectacle
- du coup le site / serveur s'inscrit dans cette volonté de rendre public et d'échanger 
- aussi d'autres envies rencontres, échanges, publications, au travers web et autres

## site

git/
    projet1/
        amuse-bouches/
            img.jpg
            video.mp3
            ...
        index.html
        script.py
        ...

site
    projets
        diapo amuse-bouches
        reprend la main
        tu te balade
        tu tripote

# material, installation

2 (7) ordis 
2 (2) videoproj
1 (2) tv 
2 (3) écrans

{ - [ ] waybackscreenshots.jpeg
{ print 
{ - [ ] alarecherchedudesignperdu.html
{ ordi 

{ - [ ] 21rms.html
{ ordi

{ - [ ] weaver.html 
{ - [ ] trames.mp4 écran ordi
{ - [ ] multidim.html
{ - [ ] timeflood.html
{ - [ ] carres.html
{ - [ ] mots.html
{ - [ ] ronds.html
{ - [ ] beau-travail.mp4
{ - [ ] multi-dir-text.mp4
{ 1 videoproj 2 tv 1 ecran 1 ordi

{ - [ ] essay.py 
{ - [ ] script.py 
{ - [ ] scrapost.py
{ - [ ] activearchive.html
{ - [ ] commentary.md
{ 2 ordi + 1 videoproj + 1 ecran + print 

{ - [ ] Licence.html 
{ print 10 exemplaires

{ - [ ] technopeople.html
{ - [ ] gooddesignisgood.html
{ 1 ordi

{ - [ ] fbwriter diaporama
{ ecran

{ - [ ] newPlenty.mp4
{ ordi
