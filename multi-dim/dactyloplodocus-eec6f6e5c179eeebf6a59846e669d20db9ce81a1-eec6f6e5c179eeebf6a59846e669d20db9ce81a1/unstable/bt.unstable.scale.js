Scale = function(options){
	define(this,options,{
		datas:{},
		context:[],
		floods:{}
	});
	this.context=[this].concat(this.context);
	this.datas[this.class]=true;
	if(this.dom)this.createDOM();
}

Scale.prototype.buildIn = function(howmany,what){
	if(!this[what.class])this[what.class]=[];
	what.context=this.context;
	what.position=what.context[0][what.class].length;
	this[what.class].push(new Scale(what));
	howmany--;
	if(!howmany)return true;
	return this.buildIn(howmany,what);
}

Scale.prototype.fill = function(options){
	this.floods[options.id]=true;
	if(options.data)this.datas[options.data]=true;
	if(this.dom)this.updateDOM();
	return true
}

function parseFloodFill(options,setting){
	if(!setting)setting=floodFills['default'];
	return fill=define({},options,setting);
}

Scale.prototype.floodFill = function(options){
	define(options,options,parseFloodFill({}));
	if(!options.id)options.id=Math.random();
	if(this.floods[options.id] || !this.fill(options) || !options.lifeTime)return false;
	options.lifeTime--;
	var relays=[];
	for(var d in options.dir){
		if(this.findRelay(options.dir[d]))relays.push(this.findRelay(options.dir[d]));
	}
	setTimeout(function(){
	for(var r in relays){
		//console.log(relays[r]);
		relays[r].floodFill(options);
	}
},17);
	return true
}

Scale.prototype.findRelay = function(dir,dim,position){
	if(!dim)dim=0;
	if(dim>dir.length-1)return position
		//console.log(this.context[this.context.length-dim-1][this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position]);
		var parent = this.context[this.context.length-dim-1][this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position];
	if(parent){
		if(!position){
			position=parent;
		}else{
			position=position[this.context[this.context.length-dim-2].class][dir[dir.length-dim-1]+this.context[this.context.length-dim-2].position];
		}
	}else{
		return false
	}
	dim++;
	return this.findRelay(dir,dim,position)
}