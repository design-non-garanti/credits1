var scales=[];
Scale = function(options){
	define(this,options,{
		name:scales.length,
		datas:{},
		floods:{},
		inventory:[],
		capacity:1,
		flood:{lifeTime:2},
		lock:0,
	});
	this.loop = new Loop(this);
	this.loop.event=function(){
		/*
		if(this.context.datas['lightSource']){
			var flood = this.context.flood;
			flood.data='night';
			flood.bool=false;
			console.log('lightSource');
			this.context.floodFill(flood);
		}*/
		if(this.context.inventory.length==0)return false
			if(this.context.flood.delay){
				this.delay=this.context.flood.delay;
			//console.log(this.context.flood.delay);
			this.context.delay=false;
		}
		if(this.context.inventory.length>0 && this.context.datas['emitter']){
			for(var i in this.context.inventory){
				if(this.context.inventory[i]=='life')this.context.flood.lifeTime++;
				//if(this.context.inventory[i]=='lock')this.context.lock++;
				if(this.context.inventory[i]=='space')this.context.capacity++;
				if(this.context.inventory[i]=='memory'){this.context.datas['night']='acid';this.context.datas['lightSource']=true};
			}
			var floodFill = this.context.flood;
			floodFill.data=this.context.inventory[this.context.lock];
			if(this.context.datas['lightSource']){
				floodFill.data='day';
				this.context.flood.lifeTime++;
			}
			if(this.context.datas[floodFill.data]=='acid'){
				floodFill.bool=false;
			}else{
				floodFill.bool=true;
			}
			floodFill.from=this.context.name;

			if(!nonEmittable[floodFill.data]){
				this.context.floodFill({data:'push',speed:17,lifeTime:parseInt(this.context.flood.lifeTime)});
				var scale=this.context;
				setTimeout(function(){
					scale.floodFill({data:'push',speed:17,bool:false,lifeTime:parseInt(scale.flood.lifeTime)});
				},100);
				this.context.floodFill(floodFill);
			}
			this.context.inventory.splice(this.context.lock,1);
		}
	}
	scales.push(this);
	if(this.dom)this.updateDOM();
	return true;
}
Scale.prototype.build = function(array,length,new_matrix){
	if(this[array].length>length-1)return this[array];
	this[array].push(new_matrix());
	var matrix = this[array][this[array].length-1];
	matrix.class=array;
	matrix.id=this[array].length-1;
	matrix.context=this;
	this.datas[this.class]=true;
	matrix.datas[matrix.class]=true;
	if(matrix.dom)this.buildDOM(matrix.dom,matrix);
	return this.build(array,length,new_matrix);
}

Scale.prototype.buildDOM = function(dom,matrix){
	if(matrix.dom instanceof HTMLElement == false)matrix.dom=document.createElement(dom);
	if(this.dom instanceof HTMLElement == false)this.dom=document.createElement(this.dom);
	this.dom.appendChild(matrix.dom);
	matrix.updateDOM();
	this.updateDOM();
	return true;
}

Scale.prototype.updateDOM = function(){
	classes='';
	for(var i in this.datas){
		if(this.datas[i]==true || truth[this.datas[i]])classes+=i+' ';
	}
	this.dom.className=classes;
	return true
}

var nonPullable={
	//'receiver':true,
	//'emitter':true,
	'mobile':true,
	'pull':true,
	'push':true,
	'type':true,
	'day':true,
	'night':true,
	'lightSource':true,
	'make':true,
'eat':true,
}
var nonIngredient={
	'receiver':true,
	'emitter':true,
	'make':true,
'eat':true,
}
var nonReceivable={
	'mobile':true,
	'jump':true,
	'receiver':true,
	'emitter':true,
	'pull':true,
	'push':true,
	'type':true,
	'money':true,
	'make':true,
	'choose':true,
	'day':true,
	'night':true,
	'eat':true,
}
var nonEmittable={
	'life':true,
	'space':true,
	'lock':true,
	'jump':true,
	'memory':true,
}
var truth={
	'sugar':true
}

Scale.prototype.fill = function(options){
	//setup
	options=Fill(options);
	if(options.data==undefined)return false;
	for(var i in options.scope){
		if(this.datas[options.scope[i]])return false
	}
if(options.id)this.floods[options.id]=true;
if(options.data=='emitter' && this.loop.stop){
	this.loop.break();
}	

	//event
	if(options.hard){
		this.datas[options.data]=options.bool;
		this.updateDOM();
		return true
		console.log('HARD');
	}
	var data=false;
	if(this.datas[options.data]=='sugar'){
		if(!options.bool)data=options.data;
	}else{
		if(this.datas[options.data]=='acid'){
			if(options.bool)data=options.data;
		}else{
			if(this.datas[options.data]=='obstacle'){
				if(options.from != this.name)return false
			}else{
				if(this.datas[options.data]=='phobic'){
					
				}else{
					if(options.bool===false && options.owner && this.datas[options.data] && !nonPullable[options.data]){
						if(options.owner.capacity>options.owner.inventory.length){
							options.owner.inventory.push(options.data);	
						}
						if(options.data=='receiver'){
							this.inventory=[];
							this.capacity=1;
							if(this.datas['lightSource']){
								this.datas['night']=false;
								this.datas['lightSource']=false;
								var position=this
									this.floodFill({data:'day',scope:['lightSource'],bool:false,lifeTime:this.flood.lifeTime+2});
									this.floodFill({data:'night',scope:['lightSource'],bool:true,lifeTime:this.flood.lifeTime+2});
								console.log('light off MERDE');
							}
						}
						if(options.data=='emitter'){
							this.flood={lifeTime:2};
							this.lock=0;
							if(this.datas['lightSource']){
								this.datas['night']=false;
								this.datas['lightSource']=false;
								var position=this
								this.floodFill({data:'day',scope:['lightSource'],bool:false,lifeTime:this.flood.lifeTime+2});
								this.floodFill({data:'night',scope:['lightSource'],bool:true,lifeTime:this.flood.lifeTime+2});
								console.log('light off PUTAIN');
							}
						}	
					}
					this.datas[options.data]=options.bool;
					if(options.bool)data=options.data;
				}
			}
		}
	}
	
	if(data && options.from!=this.name && this.datas['receiver'] && this.inventory.length<this.capacity && !nonReceivable[data]){
		this.inventory.push(data);
		this.floodFill({data:options.data,bool:false,lifeTime:1});
		this.floodFill({data:'eat',speed:17,lifeTime:2});
		var scale=this;
		setTimeout(function(){
			scale.floodFill({data:'eat',speed:17,bool:false,lifeTime:2});
		},100);
	}
	this.updateDOM();
	return true
}

var floodFills=0;

Scale.prototype.floodFill = function(options){
	options=Fill(options);
	if(options.id==undefined){
		options.id='ff'+floodFills;
		floodFills++;
	};
	options.lifeTime--;
	if(this.floods[options.id])return false
		if(options.bool==false && options.owner){
			if(options.owner.inventory.length==options.owner.capacity)return false;
		}
		if(options.bool===true && options.owner){

			options.data=pick(options.owner,this.datas);
			if(!options.data)return false
		}
	if(!this.fill(options) || !options.lifeTime)return false
		var voisins=[];
	for(var i in options.dir){
		var parent = this.context.context[this.context.class][this.context.id+options.dir[i][1]];
		if(parent){
			var sibling=parent[this.class][this.id+options.dir[i][0]];
			if(sibling){
				if(options.rdm){
					if(Math.random()>options.rdm){
						voisins.push(sibling);
					}
				}else{
					voisins.push(sibling);
				}
			}
		}
	}
	var scale = this;
	//this.dom.className='flood'
	setTimeout(function(){
		if(scale.datas[options.data]==options.bool || scale.datas[options.data]=='obstacle' || scale.datas[options.data]=='acid' || scale.datas[options.data]=='sugar'){
			for(var i in voisins){
				voisins[i].floodFill(options);
			}
		}
	},options.speed);
	return true
}

function Fill(params){
	obj={};
	return define(obj,params,{
		text:false,
		lifeTime:Infinity,
		bool:true,
		speed:100,
		hard:false,
		scope:[],
		dir:[[-1,0],[+1,0],[0,-1],[0,+1]],
		rdm:false,
	});
}
