console.clear();

console.log('webaudioapi');

var cssColors=['tan','peru','fuchsia','aquamarine','cadetblue','violet','indigo','sienna','olive','steelblue','maroon','slategrey','indianred','tomato','mediumblue','royalblue','sandybrown','skyblue','rebeccapurple','springgreen','khaki','grey','firebrick','deeppink','salmon','blue','dimgray','mediumaquamarine','blueviolet','thistle','chartreuse','teal','coral','plum','forestgreen','lawngreen','navy','lime','purple','hotpink','midnightblue','pink','mediumseagreen','slategray','burlywood','olivedrab','greenyellow','mediumslateblue','magenta','mediumturquoise','snow','silver','azure','cornflowerblue','ivory','wheat','goldenrod','chocolate','slateblue','limegreen','red','moccasin','yellowgreen','green','mediumpurple','orange','deepskyblue','saddlebrown','dimgrey','gold','mediumspringgreen','rosybrown','crimson','orchid','peachpuff','mistyrose','powderblue','brown','cyan','dodgerblue','turquoise','mediumorchid','seagreen','mediumvioletred','gray','aqua','orangere','black'];

var cssGrey=[
'#000000',
'#111111',
'#222222',
'#333333',
'#444444',
'#555555',
'#666666',
'#777777',
'#888888',
'#999999',
'#AAAAAA',
'#BBBBBB',
'#CCCCCC',
'#DDDDDD',
'#EEEEEE', 
'#FFFFFF']

function random(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min)
	}
}

//var visualisation=undefined;
//if (document.getElementById('visualisation')) {
//    visualisation = document.getElementById('visualisation');
//}
//console.log(document.getElementById('visualisation'));

// # UTILS //

function average(array) {
    var total = 0;
    for(var i = 0; i < array.length; i++) {
        total += array[i];
    }
    var average = total / array.length;
    return average;
}

function croix(newOn,number,on) {
    var newNumber = newOn*number/on;
    return newNumber;
}

function between(min,max) {
    var interval = max-min;
    var value = min+interval*moy/100;
    return value;
}

// # WEBAUDIO //

// create audio context

// start microphone
function start_microphone(stream){

    analyser_node = audioContext.createAnalyser();
    analyser_node.fftSize = 32;
    //analyser_node.fftSize = 256;
    //analyser_node.fftSize = 2048;
    analyser_node.smoothingTimeConstant = 0;
    //analyser_node.minDecibels = -30;
    //analyser_node.maxDecibels = -100;
    
    //analyser_node.connect(audioContext.destination);
    console.log(analyser_node);

    microphone_stream = audioContext.createMediaStreamSource(stream);
    microphone_stream.connect(analyser_node);
    console.log(microphone_stream);
    
    var frequencyData = new Uint8Array(analyser_node.frequencyBinCount);
    analyser_node.getByteFrequencyData(frequencyData);

   
    
//    if (visualisation) { 
//        var frequenciesDiv = document.getElementById("frequencies");
//        var barSpacingPercent = 100 / analyser_node.frequencyBinCount;
//        for (var i = 0; i < analyser_node.frequencyBinCount; i++) {
//            bar = document.createElement("div");
//            bar.style.left = i* barSpacingPercent + "%";
//            frequenciesDiv.appendChild(bar);
//        }
//    }

    update_son();
}

var  moyArr = [];

function update_son() {
    requestAnimationFrame(update_son);

    var frequencyData = new Uint8Array(analyser_node.frequencyBinCount);
    analyser_node.getByteFrequencyData(frequencyData);

    // moy
    moy = average(frequencyData);
    moy1 = 1*moy/255;       // moyenne sur 1
    moy10 = 10*moy/255;   // moyenne sur 100
    moy100 = 100*moy/255;   // moyenne sur 100
    moy16 = 16*moy/255;   // moyenne sur 16
    //console.log(moy);
    //console.log(moy1);
    //console.log(moy10);
    //console.log(moy100);

    //moy arr 
    sensitivity = 1.1;
    moyArr.push(moy);
    moyMoyArr = average(moyArr);
    console.log(moyMoyArr);
    if ( moyArr.length > 100) {
        moyArr.shift();
    }
    
    // moy arr 
    //if (visualisation) { 
        if (moy > moyMoyArr*sensitivity) {
    //        var picDiv = document.getElementById('pic');
    //        picDiv.style.backgroundColor = 'blue';
            pic = true;
            console.log(pic);
            picColor = cssColors[random(0,cssColors.length).int];
        } else {
    //        var picDiv = document.getElementById('pic');
    //        picDiv.style.backgroundColor = 'white';
            pic = false;
            console.log(pic);
        }
    //}
    //
    //
   
    console.log(picColor);

    // frequencies
    //if (visualisation) { 
    //    var frequenciesDiv = document.getElementById("frequencies");
    //    bars = frequenciesDiv.childNodes;
    //    for (i = 0; i < bars.length; i ++) {
    //        bars[i].style.height = frequencyData[i] + 'px';
    //    }
    //}

    // average
    //if (visualisation) { 
    //    var averageDiv = document.getElementById("average");
    //    averageDiv.style.top = moy + 'px';
    //}
   
    // color hsl
    var h = moy;
    var s = moy; 
    var l = moy;
    var hslColor = "hsl("+h+","+s+"%,"+l+"%)"
    floodColor = cssColors[random(0,cssColors.length).int];
    //console.log(floodColor);

    // color hex
    var hexColor = '';

    //if (visualisation) {
    //    var colorDiv = document.getElementById('color');
    //    colorDiv.style.backgroundColor = hslColor;
    //}

    //speed = average;
    speed = parseInt(1000 / moy);
    if (speed === parseInt(speed, 10)){
        //alert("data is integer")
    }
    else{
       // alert("data is not an integer")
        speed=1000;
    }
    speed*=5;
    
};

console.clear();

console.log('webaudioapi');

var cssColors=['tan','peru','fuchsia','aquamarine','cadetblue','violet','indigo','sienna','olive','steelblue','maroon','slategrey','indianred','tomato','mediumblue','royalblue','sandybrown','skyblue','rebeccapurple','springgreen','khaki','grey','firebrick','deeppink','salmon','blue','dimgray','mediumaquamarine','blueviolet','thistle','chartreuse','teal','coral','plum','forestgreen','lawngreen','navy','lime','purple','hotpink','midnightblue','pink','mediumseagreen','slategray','burlywood','olivedrab','greenyellow','mediumslateblue','magenta','mediumturquoise','snow','silver','azure','cornflowerblue','ivory','wheat','goldenrod','chocolate','slateblue','limegreen','red','moccasin','yellowgreen','green','mediumpurple','orange','deepskyblue','saddlebrown','dimgrey','gold','mediumspringgreen','rosybrown','crimson','orchid','peachpuff','mistyrose','powderblue','brown','cyan','dodgerblue','turquoise','mediumorchid','seagreen','mediumvioletred','gray','aqua','orangere','black'];

var cssGrey=[
'#000000',
'#111111',
'#222222',
'#333333',
'#444444',
'#555555',
'#666666',
'#777777',
'#888888',
'#999999',
'#AAAAAA',
'#BBBBBB',
'#CCCCCC',
'#DDDDDD',
'#EEEEEE', 
'#FFFFFF']

function random(min, max) {
	return {
		float:Math.random() * (max - min) + min,
		int:parseInt(Math.random() * (max - min) + min)
	}
}

//var visualisation=undefined;
//if (document.getElementById('visualisation')) {
//    visualisation = document.getElementById('visualisation');
//}
//console.log(document.getElementById('visualisation'));

// # UTILS //

function average(array) {
    var total = 0;
    for(var i = 0; i < array.length; i++) {
        total += array[i];
    }
    var average = total / array.length;
    return average;
}

function croix(newOn,number,on) {
    var newNumber = newOn*number/on;
    return newNumber;
}

function between(min,max) {
    var interval = max-min;
    var value = min+interval*moy/100;
    return value;
}

// # WEBAUDIO //

// create audio context
var audioContext = new (window.AudioContext || window.webkitAudioContext)();

if (navigator.getUserMedia){
  navigator.getUserMedia({audio:true}, 
        function(stream) {
            start_microphone(stream);
        },
        function(e) {
            alert('Error capturing audio.');
        }
        );
} else { 
    alert('getUserMedia not supported in this browser.'); 
}

// start microphone
function start_microphone(stream){

    analyser_node = audioContext.createAnalyser();
    analyser_node.fftSize = 32;
    //analyser_node.fftSize = 256;
    //analyser_node.fftSize = 2048;
    analyser_node.smoothingTimeConstant = 0;
    //analyser_node.minDecibels = -30;
    //analyser_node.maxDecibels = -100;
    
    //analyser_node.connect(audioContext.destination);
    console.log(analyser_node);

    microphone_stream = audioContext.createMediaStreamSource(stream);
    microphone_stream.connect(analyser_node);
    console.log(microphone_stream);
    
    var frequencyData = new Uint8Array(analyser_node.frequencyBinCount);
    analyser_node.getByteFrequencyData(frequencyData);

   
    
//    if (visualisation) { 
//        var frequenciesDiv = document.getElementById("frequencies");
//        var barSpacingPercent = 100 / analyser_node.frequencyBinCount;
//        for (var i = 0; i < analyser_node.frequencyBinCount; i++) {
//            bar = document.createElement("div");
//            bar.style.left = i* barSpacingPercent + "%";
//            frequenciesDiv.appendChild(bar);
//        }
//    }

    update_son();
}

var  moyArr = [];

function update_son() {
    requestAnimationFrame(update_son);

    var frequencyData = new Uint8Array(analyser_node.frequencyBinCount);
    analyser_node.getByteFrequencyData(frequencyData);

    // moy
    moy = average(frequencyData);
    moy1 = 1*moy/255;       // moyenne sur 1
    moy10 = 10*moy/255;   // moyenne sur 100
    moy100 = 100*moy/255;   // moyenne sur 100
    moy16 = 16*moy/255;   // moyenne sur 16
    //console.log(moy);
    //console.log(moy1);
    //console.log(moy10);
    //console.log(moy100);

    //moy arr 
    sensitivity = 1.1;
    moyArr.push(moy);
    moyMoyArr = average(moyArr);
    //console.log(moyMoyArr);
    if ( moyArr.length > 100) {
        moyArr.shift();
    }
    

    // moy arr 
    //if (visualisation) { 
        if (moy > moyMoyArr*sensitivity) {
    //        var picDiv = document.getElementById('pic');
    //        picDiv.style.backgroundColor = 'blue';
            pic = true;
            //console.log('PIC');
            picColor = cssColors[random(0,cssColors.length).int];
        } else {
    //        var picDiv = document.getElementById('pic');
    //        picDiv.style.backgroundColor = 'white';
            pic = false;
        }
    //}
    //
    //
   
    //console.log(picColor);

    // frequencies
    //if (visualisation) { 
    //    var frequenciesDiv = document.getElementById("frequencies");
    //    bars = frequenciesDiv.childNodes;
    //    for (i = 0; i < bars.length; i ++) {
    //        bars[i].style.height = frequencyData[i] + 'px';
    //    }
    //}

    // average
    //if (visualisation) { 
    //    var averageDiv = document.getElementById("average");
    //    averageDiv.style.top = moy + 'px';
    //}
   
    // color hsl
    var h = moy;
    var s = moy; 
    var l = moy;
    var hslColor = "hsl("+h+","+s+"%,"+l+"%)"
    floodColor = cssColors[random(0,cssColors.length).int];
    //console.log(floodColor);

    // color hex
    var hexColor = '';

    //if (visualisation) {
    //    var colorDiv = document.getElementById('color');
    //    colorDiv.style.backgroundColor = hslColor;
    //}

    //speed = average;
    speed = parseInt(1000 / moy);
    if (speed === parseInt(speed, 10)){
        //alert("data is integer")
    }
    else{
       // alert("data is not an integer")
        speed=1000;
    }
    speed*=5;
    
};



