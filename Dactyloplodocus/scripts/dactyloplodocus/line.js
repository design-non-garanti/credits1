/*
                 ( line.js )
               __ |/
              / _)
     _.----._/ /
    /         /
 __/ (  | (  |
/__.-'|_|--|_|

*/

Line = function(options){
    define(this,options,{
        id:undefined,
        types:[],
        matrix:undefined,
        dom:document.createElement('tr')
    });
}

Line.prototype.get_content = function (index){

    var full='';
    var script='';
    var before=[-1];
    var after='';

    for(var i in this.types){
        if(this.types[i].face != '\\' && this.types[i].face != '/' && this.types[i].face != '$' && this.types[i].face != '#' && this.types[i].face != '%' && this.types[i].face != ' ')script+=this.types[i].face;
        full+=this.types[i].face;
        if(i<index && this.types[i].face != ' ' && this.types[i].face != ' '){
            //console.log(this.types[i].face);
            before.push(i);
        }
        if(i>index && this.types[i].face != ' ' && this.types[i].face != ' '){
            after+=this.types[i].face;
        }
    }

    return {
        script:script,
        full:full,
        before:before,
        after:after,
    }

}

Line.prototype.put_content = function (what,where,ink){
    var content=this.get_content().full;
    var start=content.slice(0,where);
    var end=content.slice(where);
    var new_content=start+what+end;
    for(var i in this.types){
     this.types[i].fill({face:new_content[i],ink:ink});
 }
}

Line.prototype.sub_content = function (where){
    var content=this.get_content().full;
    var start=content.slice(0,where-1);
    var end=content.slice(where);
    var new_content=start+end+' ';
    for(var i in this.types){
     this.types[i].fill({face:new_content[i]});
 }
}