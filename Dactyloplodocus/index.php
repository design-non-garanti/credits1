<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" /> 
    <title>q</title>
    <script type="text/javascript" src="assets/font8x8.js" defer></script>
    <script type="text/javascript" src="scripts/colors.js" defer></script>
    <?php
    $dir = "scripts/dactyloplodocus/";
    $fonts = scandir($dir);

    foreach ($fonts as $key => $value) {
        if($value == "." || $value == ".."){}else{
            echo "<script type=\"text/javascript\" src=\"".$dir.$value."\" defer></script>";
        }
    }
    ?>
    <link rel="stylesheet" type="text/css" href="styles/styles.css">
</head>
<body>
        <script type="text/javascript" src="scripts/init.js" defer></script>
               <h1>Click to start</h1>
</body>
</html>
